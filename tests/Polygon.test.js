import Polygon from '../client/Polygon.js';

describe("Polygon", () => {
    describe("pointInsidePolygon", () => {
        it("should indicate point not inside polygon for simple polygon", () => {
            const point = { x: 0, y: 10 };
            // a triangle
            const polygon = [
                { x: 5, y: 5 },
                { x: 15, y: 15 },
                { x: 5, y: 15 },
            ];

            const result = Polygon.pointInsidePolygon(point, polygon);
            expect(result).toBe(false);
        });

        it("should indicate point inside polygon for simple polygon", () => {
            const point = { x: 10, y: 10 };
            // a triangle
            const polygon = [
                { x: 5, y: 5 },
                { x: 15, y: 15 },
                { x: 5, y: 15 },
            ];

            const result = Polygon.pointInsidePolygon(point, polygon);
            expect(result).toBe(true);
        });
    });
});