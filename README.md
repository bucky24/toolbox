# Toolbox

This is intended to be a collection of misc things that I've had to do over and over again. It is small but will hopefully continue to grow.

## Frontend

Import the frontend code by using:

```
import { Polygon } from '@bucky24/toolbox/client';
```

## Backend

Import the backend code by using:

```
const { Api } = require('@bucky24/toolbox/server');
```

## Classes/Components

Note components end with `.jsx`. Make sure if you are using webpack that you handle this file extension accordingly.

### Polygon (Frontend)

The Polygon class contains methods for manipulating and getting data about polygons.

#### pointInsidePolygon

This method returns a boolean indicating if the given Point is inside the given Polygon

| param | type | description |
| -- | -- | -- |
| point | Point | The point to check against the polygon |
| polygon | Polygon | The Polygon to check |

### Api (Frontend)

The Api class contains methods for calling api data

#### callApi

This method returns a Promise resolving to the result. This method expects to receive JSON as a response.

| param | type | description |
| -- | -- | -- |
| getBaseUrlFn | Function | This is run every call to determine what the base url (hostname) is |
| method | String | This is expected to be an HTTP method, all uppercase |
| url | String | The url to add to the base url |
| data | Object | An optional object containing data to send with the API request |
| headers | Object| An optional object containing headers to send |

### Auth (Frontend)

The Auth class on the Frontend handles making authenticated requests with previously stored data

#### authedFetch

This method calls `Api.callApi` with an `Authentication` header mapping to a Bearer token. The parameters are the same as `Api.callApi`

### Auth (Backend)

The Auth class on the Backend handles registering and validating auth tokens.

#### createToken

This method can take in some data, create a JWT token, and send it both as a cookie, and as a `token` field in the JSON response to the client.

| param | type | description |
| -- | -- | -- |
| res | Express Response | The response object for the api |
| data | Object | The object to embed in JWT |
| timeoutSec | Integer | The timestamp of how long the token should last. Defaults to 1 hour |

#### checkAuth

This method is an Express middleware. It verifies a valid token exists on either the `session` cookie, or as part of the `Authentication` header. It will place the extracted data on `req.authData`.

### AuthContext (Frontend)

The AuthContext (and AuthProvider) handle token storage and retrieval

#### Variables

| variable | type | description |
| -- | -- | -- |
| loading | Boolean | True while system is being initalized |
| loggedIn | Boolean | True if a valid session token is found |

#### setToken

This method saves the token to localstorage

| param | type | description |
| -- | -- | -- | 
| token | String | Token to store |

#### authedFetch

This method is basically the same as `Auth.authenticatedFetch`.

#### logout

This method provides functionality to handle clearing tokens.

### Router

The Router component wraps routes for `react-router-dom` adding in the distinction between routes that need a logged in user and normal routes.

| prop | type | description |
| -- | -- | -- | 
| routes | RouteData[] | List of routes that can be accessed without auth |
| authedRoutes | RouteData[] | List of routes that can be accessed with auth |
| loader | Component | Component to display while page is loaded |
| loginRoute | String | Route to redirect to when the user tries to access an authenciated route without proper auth |

## Types

### Point

A point is a simple object that contains a numeric x and a numeric y.

### Polygon

The Polygon type is simply an array of Points.

### RouteData

A RouteData object contains the following properties

| prop | type | description |
| -- | -- | -- | 
| path | String | Path to the route |
| element | React Element | The element to render for the route |
