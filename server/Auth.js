const jwt = require("jsonwebtoken");

const ONE_HOUR = 60 * 60;

function createToken(res, data, timeoutSec = ONE_HOUR) {
    if (!process.env.JWT_KEY) {
        throw new Error("JWT_KEY not found in process.env");
    }
    const token = jwt.sign(data || {}, process.env.JWT_KEY, { expiresIn: timeoutSec });

    res.cookie("session", token, { maxAge: timeoutSec * 1000 });

    res.status(200).json({
        token,
    });
}

function checkAuth(req, res, next) {
    if (!process.env.JWT_KEY) {
        throw new Error("JWT_KEY not found in process.env");
    }

    let session;
    if (req.cookies) {
        session = req.cookies["session"];
    }
    if (!session) {
        session = req.header("Authentication");
        if (session) {
            const [_, token] = session.split("Bearer ");
            session = token;
        }
    }

    if (!session) {
        res.status(401).end();
        return;
    }

    try {
        const data = jwt.verify(session, process.env.JWT_KEY);

        req.authData = data;
        next();
    } catch (e) {
        console.log(e);
        res.status(403).end();
        return;
    }
}

module.exports = {
    createToken,
    checkAuth,
};