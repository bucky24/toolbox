import { Routes, Route, useNavigate } from 'react-router-dom';
import React from 'react';

import AuthedRoute from './AuthedRoute.jsx';

export default function Router({ routes, authedRoutes, loader, loginRoute }) {
    const navigate = useNavigate();
    const routeElements = [];

    routes.forEach(({ path, element }) => {
        routeElements.push(<Route key={path} path={path} element={element} />);
    });
    
    authedRoutes.forEach(({ path, element }) => {
        routeElements.push(<Route key={path} path={path} element={<AuthedRoute
            loader={loader}
            onAuthFailure={() => navigate(loginRoute)}
        >{element}</AuthedRoute>} />);
    });

    return <Routes>{routeElements}</Routes>;
}