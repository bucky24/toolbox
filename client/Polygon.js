
export default class Polygon {
    /**
     * Returns true if a point is inside a polygon
     * This method was adapated from code found at http://alienryderflex.com/polygon/
     * 
     * @param {Point} point the point to check against
     * @param {Point[]} polygon the polygon to check points against
     */
    static pointInsidePolygon(point, polygon) {
        const { x, y } = point;
        let oddNodes = false;
        let j = polygon.length-1;
        for (let i=0;i<polygon.length;i++) {
            const px1 = polygon[i].x;
            const py1 = polygon[i].y;
            const px2 = polygon[j].x;
            const py2 = polygon[j].y;
            if ((py1 < y && py2 >= y) || (py2 < y && py1 >= y)) {
                if (px1 + (y-py1)/(py2-py1)*(px2-px1) < x) {
                    oddNodes = !oddNodes;
                }
            }
            j = i;
        }
        return oddNodes;
    }
}