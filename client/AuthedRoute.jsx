import React, { useContext, useEffect} from 'react';

import AuthContext from './AuthContext.jsx';

export default function AuthedRoute({ children, onAuthFailure }) {
    const { loggedIn, loading } = useContext(AuthContext);

    useEffect(() => {
        if (!loading && !loggedIn) {
            onAuthFailure();
        }
    }, [loggedIn, loading]);

    if (loading) {
        return <div>Loading</div>;
    }

    if (!loggedIn) {
        return null;
    }

    return children;
}