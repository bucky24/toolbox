import React, { useState, useEffect } from 'react';
import * as Auth from './Auth';

const AuthContext = React.createContext({});
export default AuthContext;

export function AuthProvider({ children }) {
    const [loading, setLoading] = useState(true);
    const [loggedIn, setLoggedIn] = useState(false);

    const checkLogin = () => {
        const session = localStorage.getItem("session");
        if (session) {
            setLoggedIn(true);
        }
        setLoading(false);
    }

    useEffect(checkLogin, []);

    const value = {
        loading,
        loggedIn,
        setToken: (token) => {
            localStorage.setItem("session", token);
            checkLogin();
        },
        authedFetch: (getUrl, method, api, data) => {
            return Auth.authedFetch(getUrl, method, api, data);
        },
        logout: () => {
            localStorage.clear("session");
            setLoggedIn(false);
        }
    };

    return <AuthContext.Provider value={value}>
        {children}
    </AuthContext.Provider>
}