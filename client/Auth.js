import { callApi } from './Api';

export function authedFetch(getBaseUrlFn, method, url, data, headers) {
    const token = localStorage.getItem("session");

    const newHeaders = {...headers};

    if (token) {
        newHeaders.Authentication = `Bearer ${token}`;
    }

    return callApi(getBaseUrlFn, method, url, data, newHeaders);
}