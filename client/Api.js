export async function callApi(getBaseUrlFn, method, url, data, headers) {
    const baseUrl = getBaseUrlFn();

    const fullURI = baseUrl + (url.startsWith("/") ? "" : "/") + url;

    let fullURL = fullURI;

    const options = {
        method,
        headers: {
            ...headers,
        },
    };

    data = data || {};

    if (method === "GET") {
        const queryList = [];
        Object.keys(data).forEach((key) => {
            let value = data[key];
            if (typeof value === "object") {
                value = JSON.stringify(value);
            }
            queryList.push(`${key}=${value}`);
        });

        if (queryList.length > 0) {
            fullURL += "?" + queryList.join("&");
        }
    } else if (method === "POST" || method === "PUT") {
        options.headers['Content-Type'] = "application/json";
        options.body = JSON.stringify(data);
    }

    const result = await fetch(fullURL, options);
    if (!result.ok) {
        console.error("Result got " + result.status + " for " + result.url);
        return null;
    }

    const json = await result.json();

    return json;
}